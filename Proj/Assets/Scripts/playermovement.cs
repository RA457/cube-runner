﻿using UnityEngine;

public class playermovement : MonoBehaviour
{
    public float forceforword = 2000f;
    public float dirfor = 600f;
    public Rigidbody rb;
    void FixedUpdate()
    {
        rb.AddForce(0, 0,forceforword*Time.deltaTime);
        if( Input.GetKey("d") )
        {
            rb.AddForce(dirfor * Time.deltaTime,0,0,ForceMode.VelocityChange);
        }
        if ( Input.GetKey("a") )
        {
            rb.AddForce(-dirfor * Time.deltaTime, 0, 0, ForceMode.VelocityChange);
        }
        if (Input.GetKey("o"))
        {
            rb.AddForce(0, 70 * Time.deltaTime, 0, ForceMode.VelocityChange);
        }
        if (Input.GetKey("q"))
        {
            rb.AddForce(0, 0, 250);
        }
        if (Input.GetKey("l"))
        {
            rb.AddForce(0, -70 * Time.deltaTime, 0, ForceMode.VelocityChange);
        }
        if (rb.position.y < -5f)
        {
            FindObjectOfType<GameManager>().EndGame(); 
        }
    }
}
